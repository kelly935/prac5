#include "ReduceGeneric.h"
#include <iostream>

int ReduceGeneric::reduce(std::vector<int> vec)
{
    //Base case where vec is a two elements
    if(vec.size() == 2)
    {
        return binaryOperator(vec[0], vec[1]);
    }

    //Base case where vec is a single element
    if(vec.size() == 1)
    {
        return vec[0];
    }

    //Split the vector
    VecPair vp = vecSplit(vec);

    //Call reduce on Split Vectors
    int numLeft = reduce(vp.vecLeft);
    int numRight = reduce(vp.vecRight);

    //Reduce both integers into one and return the result
    return binaryOperator(numLeft, numRight);
}

void ReduceGeneric::vecPrint(std::vector<int> vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        //Check if we are at the end of the vector
        if(i == vec.size() - 1)
        {
            std::cout << vec[i] << std::endl;
        }
        else
        {
            std::cout << vec[i] << " ";
        }
    }
}

ReduceGeneric::VecPair ReduceGeneric::vecSplit(std::vector<int> vec)
{
    //Determine the size of left and right vectors from split
    unsigned int vecLeftSize = vec.size() / 2;

    VecPair vp;

    //Populate left vector
    for(unsigned int i = 0; i < vecLeftSize; i++)
    {
        vp.vecLeft.push_back(vec[i]);
    }

    //Populate right vector
    for(unsigned int i = vecLeftSize; i < vec.size(); i++)
    {
        vp.vecRight.push_back(vec[i]);
    }

    return vp;
}

std::vector<int> ReduceGeneric::vecMerge(ReduceGeneric::VecPair vp)
{
    //Get total vector size
    std::vector<int> vec;

    //Add left vector to total vector
    for(unsigned int i = 0; i < vp.vecLeft.size(); i++)
    {
        vec.push_back(vp.vecLeft[i]);
    }

    //Add right vector to total vector
    for(unsigned int i = 0; i < vp.vecRight.size(); i++)
    {
        vec.push_back(vp.vecRight[i]);
    }

    return vec;
}
