#include "ReduceGCD.h"

int ReduceGCD::binaryOperator(int numLeft, int numRight)
{
    if(numLeft < 0)
    {
        numLeft = -numLeft;
    }

    if(numRight < 0)
    {
        numRight = -numRight;
    }

    while(numLeft != 0 && numRight != 0)
    {
        int nextLeft, nextRight;
        //Get the greatest of the two 
        if(numLeft == numRight)
        {
            return numLeft;
        }
        else if(numLeft < numRight)
        {
            nextLeft = numLeft;
            nextRight = numRight % numLeft;
        }
        else
        {
            nextLeft = numRight;
            nextRight = numLeft % numRight;
        }

        numLeft = nextLeft;
        numRight = nextRight;
    }

    if(numLeft > numRight)
    {
        return numLeft;
    }
    
    return numRight;
}