#ifndef _FILTER_ODD_H_
#define _FILTER_ODD_H_

#include "FilterGeneric.h"

//Inherit from generic filter parent class
class FilterOdd : public FilterGeneric
{
    protected:
        //Child definition for parent's pure virtual function declaration
        virtual bool g(int num);
};

#endif