#include <vector>
#include <iostream>
#include <sstream>

#include "MapTriple.h"
#include "MapSquare.h"
#include "MapAbsoluteValue.h"

#include "FilterOdd.h"
#include "FilterNonPositive.h"
#include "FilterForTwoDigitPositive.h"

#include "ReduceMinimum.h"
#include "ReduceGCD.h"

int main()
{
    MapTriple mt;
    MapSquare ms;
    MapAbsoluteValue ma;
    FilterOdd fo;
    FilterNonPositive fn;
    FilterForTwoDigitPositive ft;
    ReduceMinimum rm;
    ReduceGCD rg;

    //Get the user input on a single line
    std::string in;
    getline(std::cin, in);

    //Use stringstream to more easily handle spaces and commas in the input
    std::stringstream stringstream(in);

    //While stringstream is not empty use it to fill up input vector
    std::vector<int> vec;
    std::string buffer;
    while(!stringstream.eof())
    {
        stringstream >> buffer;
        vec.push_back(stoi(buffer)); 
    }

    //Calculation of L'
    vec = mt.map(vec);
    vec = ma.map(vec);

    //Calculation of L"
    vec = ft.filter(vec);
    vec = fo.filter(vec);

    //Calculation of Minimum and GCD
    std::cout << rm.reduce(vec) << " " << rg.reduce(vec) << std::endl;
}