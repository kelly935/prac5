#ifndef _REDUCE_GENERIC_H_
#define _REDUCE_GENERIC_H_

#include <vector>

class ReduceGeneric
{
    public:
    
        //A struct to hold the split vector
        typedef struct
        {
            std::vector<int> vecLeft;
            std::vector<int> vecRight;

        } VecPair;

        /** Reduce method will recursivly split vector until size of one or two
         *  If a size of one just returns the element as an int. If there are
         *  two elements then the binary operator function is called on them
         *  and a single integet is returned. Binary operator is defined
         *  in each child class as it is pure virtual here.
         */
        int reduce(std::vector<int> vec);
        //A function for quickly printing out vectors
        void vecPrint(std::vector<int> vec);
    
    protected:
        /** Please note that the prac has a mistake, this function MUST be protected or public, private
         * access modifier would make it impossible for children classes to access it */

        virtual int binaryOperator(int numLeft, int numRight) = 0; //Pure Virtual (expects that parents will override this function)
        
        //A function for splitting up vectors in half and returning them as a pair
        VecPair vecSplit(std::vector<int> vec);
        //A function for merging a vector pair into a single vector
        std::vector<int> vecMerge(VecPair vp);
};

#endif