#include "FilterForTwoDigitPositive.h"

bool FilterForTwoDigitPositive::g(int num)
{
    //Check to see if positive
    if(num < 0)
    {
        return false;
    }

    //Check for two digits by checking if 10 devides into it at least once
    if(num / 10 < 1)
    {
        return false;
    }

    //Check that number does not have more than 2 digits by checking if 100 devides into it at least once
    if(num / 100 >= 1)
    {
        return false;
    }

    return true;
}