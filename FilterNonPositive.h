#ifndef _FILTER_NON_POSITIVE_H_
#define _FILTER_NON_POSITIVE_H_

#include "FilterGeneric.h"

//Inherit from generic filter parent class
class FilterNonPositive : public FilterGeneric
{
    protected:
        //Child definition for parent's pure virtual function declaration
        virtual bool g(int num);
};

#endif