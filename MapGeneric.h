#ifndef _MAP_GENERIC_H_
#define _MAP_GENERIC_H_

#include <vector>

class MapGeneric
{
    public:
    
        //A struct to hold the split vector
        typedef struct
        {
            std::vector<int> vecLeft;
            std::vector<int> vecRight;

        } VecPair;

        /** Map method will recursivly split vector until size of one
         *  and then will map to individual value using using function f 
         *  were child class will define (currently pure virtual)
         */
        std::vector<int> map(std::vector<int> vec);
        //A function for quickly printing out vectors
        void vecPrint(std::vector<int> vec);
    
    protected:
        /** Please note that the prac has a mistake, this function MUST be protected or public, private
         * access modifier would make it impossible for children classes to access it */

        virtual int f(int num) = 0; //Pure Virtual (expects that parents will override this function)
        
        //A function for splitting up vectors in half and returning them as a pair
        VecPair vecSplit(std::vector<int> vec);
        //A function for merging a vector pair into a single vector
        std::vector<int> vecMerge(VecPair vp);
};

#endif