#include "ReduceMinimum.h"

int ReduceMinimum::binaryOperator(int numLeft, int numRight)
{
    if(numLeft < numRight)
    {
        return numLeft;
    }
    return numRight;
}