#ifndef _MAP_TRIPLE_H_
#define _MAP_TRIPLE_H_

#include "MapGeneric.h"

//Inherit from generic filter parent class
class MapTriple : public MapGeneric
{
    protected:
        //Child definition for parent's pure virtual function declaration
        virtual int f(int num);
};

#endif