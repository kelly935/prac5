#include "FilterGeneric.h"
#include <iostream>

std::vector<int> FilterGeneric::filter(std::vector<int> vec)
{
    //Base case where vec is a single element
    if(vec.size() == 1)
    {
        if(g(vec[0]))
        {
            return vec;
        }

        std::vector<int> emptyVec;
        return emptyVec;
    }

    //Split the vector
    VecPair vp = vecSplit(vec);

    //Call Filter on Split Vectors
    vp.vecLeft = filter(vp.vecLeft);
    vp.vecRight = filter(vp.vecRight);

    //Combine the vector
    std::vector<int> newVec = vecMerge(vp);
    
    return newVec;
}

void FilterGeneric::vecPrint(std::vector<int> vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        //Check if we are at the end of the vector
        if(i == vec.size() - 1)
        {
            std::cout << vec[i] << std::endl;
        }
        else
        {
            std::cout << vec[i] << " ";
        }
    }
}

FilterGeneric::VecPair FilterGeneric::vecSplit(std::vector<int> vec)
{
    //Determine the size of left and right vectors from split
    unsigned int vecLeftSize = vec.size() / 2;

    VecPair vp;

    //Populate left vector
    for(unsigned int i = 0; i < vecLeftSize; i++)
    {
        vp.vecLeft.push_back(vec[i]);
    }

    //Populate right vector
    for(unsigned int i = vecLeftSize; i < vec.size(); i++)
    {
        vp.vecRight.push_back(vec[i]);
    }

    return vp;
}

std::vector<int> FilterGeneric::vecMerge(FilterGeneric::VecPair vp)
{
    //Get total vector size
    std::vector<int> vec;

    //Add left vector to total vector
    for(unsigned int i = 0; i < vp.vecLeft.size(); i++)
    {
        vec.push_back(vp.vecLeft[i]);
    }

    //Add right vector to total vector
    for(unsigned int i = 0; i < vp.vecRight.size(); i++)
    {
        vec.push_back(vp.vecRight[i]);
    }

    return vec;
}
