#ifndef _REDUCE_GCD_H_
#define _REDUCE_GCD_H_

#include "ReduceGeneric.h"

//Inherit from generic filter parent class
class ReduceGCD : public ReduceGeneric
{
    protected:
        //Child definition for parent's pure virtual function declaration
        virtual int binaryOperator(int numLeft, int numRight);
};

#endif