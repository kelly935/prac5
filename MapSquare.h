#ifndef _MAP_SQUARE_H_
#define _MAP_SQUARE_H_

#include "MapGeneric.h"

//Inherit from generic filter parent class
class MapSquare : public MapGeneric
{
    private:
        //Child definition for parent's pure virtual function declaration
        virtual int f(int num);
};

#endif