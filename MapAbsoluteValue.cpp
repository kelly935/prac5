#include "MapAbsoluteValue.h"

int MapAbsoluteValue::f(int num)
{
    if(num < 0)
    {
        return -num;
    }
    
    return num;
}