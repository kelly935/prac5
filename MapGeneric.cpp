#include "MapGeneric.h"
#include <iostream>

std::vector<int> MapGeneric::map(std::vector<int> vec)
{
    //Base case where vec is a single element
    if(vec.size() == 1)
    {
        vec[0] = f(vec[0]);
        return vec;
    }

    //Split the vector
    VecPair vp = vecSplit(vec);

    //Call Map on Split Vectors
    vp.vecLeft = map(vp.vecLeft);
    vp.vecRight = map(vp.vecRight);

    //Combine the vector
    std::vector<int> newVec = vecMerge(vp);
    
    return newVec;
}

void MapGeneric::vecPrint(std::vector<int> vec)
{
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        //Check if we are at the end of the vector
        if(i == vec.size() - 1)
        {
            std::cout << vec[i] << std::endl;
        }
        else
        {
            std::cout << vec[i] << " ";
        }
    }
}

MapGeneric::VecPair MapGeneric::vecSplit(std::vector<int> vec)
{
    //Determine the size of left and right vectors from split
    unsigned int vecLeftSize = vec.size() / 2;

    VecPair vp;

    //Populate left vector
    for(unsigned int i = 0; i < vecLeftSize; i++)
    {
        vp.vecLeft.push_back(vec[i]);
    }

    //Populate right vector
    for(unsigned int i = vecLeftSize; i < vec.size(); i++)
    {
        vp.vecRight.push_back(vec[i]);
    }

    return vp;
}

std::vector<int> MapGeneric::vecMerge(MapGeneric::VecPair vp)
{
    //Get total vector size
    std::vector<int> vec;

    //Add left vector to total vector
    for(unsigned int i = 0; i < vp.vecLeft.size(); i++)
    {
        vec.push_back(vp.vecLeft[i]);
    }

    //Add right vector to total vector
    for(unsigned int i = 0; i < vp.vecRight.size(); i++)
    {
        vec.push_back(vp.vecRight[i]);
    }

    return vec;
}
