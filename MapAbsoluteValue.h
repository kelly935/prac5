#ifndef _MAP_ABSOLUTE_VALUE_H_
#define _MAP_ABSOLUTE_VALUE_H_

#include "MapGeneric.h"

//Inherit from generic filter parent class
class MapAbsoluteValue : public MapGeneric
{
    protected:
        //Child definition for parent's pure virtual function declaration
        virtual int f(int num);
};

#endif