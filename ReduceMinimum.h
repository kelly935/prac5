#ifndef _REDUCE_MINIMUM_H_
#define _REDUCE_MINIMUM_H_

#include "ReduceGeneric.h"

//Inherit from generic filter parent class
class ReduceMinimum : public ReduceGeneric
{
    public:

    protected:
        //Child definition for parent's pure virtual function declaration
        virtual int binaryOperator(int numLeft, int numRight);
};

#endif